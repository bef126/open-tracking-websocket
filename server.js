function log_me(msg){
  var ts = new Date();
  var tss = ts.toString();
  //tss = tss.substring(0, tss.indexOf(' GMT'));
  console.log(tss + ": " + msg);
}

var express = require('express');
var http = require('http');
var app = express();
var server = http.createServer(app);

app.set('port', (process.env.PORT || 5000))
var io = require('socket.io').listen(server);
io.set('log level', 2);

io.sockets.on('connection', function (socket) {
  socket.on('join_channel', function(channel){
    log_me("join_channel: " + channel);
    socket.channel = channel;
    socket.join(channel);
  });
  socket.on('broadcast', function(data){
    io.sockets.in(data.channel).emit(data.event, data);
  });
  socket.on('disconnect', function(){
    log_me("disconnect");
    if(socket.channel){
      socket.leave(socket.channel);
    }
  });
});

server.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))
})

