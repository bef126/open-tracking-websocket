# Open Tracking Websocket

Open Tracking is an open-source mobile tracking platform (currently Android-only). The platform consists of 3 main projects:

#### Open Tracking API

[https://bitbucket.org/bef126/open-tracking-api](https://bitbucket.org/bef126/open-tracking-api)

The Open Tracking API contains the API that the Android project uses and the tracking website for displaying the phone's location on a map. It is written in [Google Go](https://golang.org) to be run on [Google App Engine](https://cloud.google.com/appengine/docs).

#### Open Tracking Websocket

[https://bitbucket.org/bef126/open-tracking-websocket](https://bitbucket.org/bef126/open-tracking-websocket)

The Open Tracking Websocket server is a simple [socket.io](http://socket.io) server. It facilitates the real-time location updates between the mobile device and the tracking website. It was designed to run on a [Heroku](http://heroku.com) dyno, but can be easily modified to run on any [Node.js](http://nodejs.org) container.

#### Open Tracking Android

[https://bitbucket.org/bef126/open-tracking-android](https://bitbucket.org/bef126/open-tracking-android)

The Open Tracking Android project contains the mobile tracking app. The tracking app allows you to start and stop tracking, as well as send email invites for people to view the tracked location on a map.

## Setup

This project was designed and tested to run on [Heroku](http://heroku.com). So you will need to create a Heroku account, then download and setup the [Heroku Toolbelt](https://toolbelt.heroku.com)

Then inside of the git repository, you can run the following command to create a new project on Heroku as well as add the `heroku` remote to your git repository:

`heroku create`

## Uploading

Assuming you already have a `heroku` origin setup, you can simply push your code to Heroku via get as follows:

`git push heroku master`

